﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrossProject.Extenders;
using UnityEngine;

namespace Sync
{
  public interface ISnapshot
  {
    float SnapshotTime { get; }
  }


  class SnapshotCollection<TSnapshot> where TSnapshot : ISnapshot
  {
    private readonly SortedList<float, TSnapshot> _snapshots;

    public SnapshotCollection()
    {
      _snapshots = new SortedList<float, TSnapshot>();
    }

    public SnapshotCollection(int capacity)
    {
      _snapshots = new SortedList<float, TSnapshot>(capacity);
    }

    public void Add(TSnapshot snapshot)
    {
      _snapshots.Add(snapshot.SnapshotTime, snapshot);
    }

    public int Count
    {
      get { return _snapshots.Count; }
    }

    public int IndexOfSnapshotBeforeTime(float timeYouWantToBack)
    {
      int firstThatCoverTimeInterval = _snapshots.Keys.IndexOf(snapshotTime => snapshotTime >= timeYouWantToBack);

      if (firstThatCoverTimeInterval - 1 < 0)
        return -1;

      return firstThatCoverTimeInterval - 1;
    }

    public void Clean(float lastLeavingSeconds = 0.8f)
    {
      float timeToTrimFrom = Time.time - lastLeavingSeconds;

      int indexToTrimFrom = IndexOfSnapshotBeforeTime(timeToTrimFrom);

      TSnapshot[] rest = _snapshots.Skip(indexToTrimFrom - 1)
                          .Select(pair=>pair.Value)
                          .ToArray();

      Clear();

      foreach (TSnapshot snapshot in rest)
        Add(snapshot);
    }

    public void Clear()
    {
      _snapshots.Clear();
    }

    public TSnapshot this[int index]
    {
      get
      {
        return _snapshots.Values.ElementAt(index);
      }
    }
  }
}
