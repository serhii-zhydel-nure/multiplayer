﻿using UnityEngine;
using UnityEngine.Networking;

namespace Sync
{
  public partial class SyncTransform
  {

    [SerializeField]
    private float _cleanInterval = 1;
    private float _nextCleanTime = 1;


    [ClientRpc]
    private void RpcSendSnapshot(TransformSnapshot snapshot)
    {
      _snapshots.Add(snapshot);
    }


    private void Update()
    {
      if (isClient)
      {
        if (Time.time >= _nextCleanTime)
        {
          _snapshots.Clean();
          _nextCleanTime += _cleanInterval;
        }

        if (!isLocalPlayer)
          InterpolateSnapshots();
      }
    }

    private void InterpolateSnapshots()
    {
      float renderTime = SyncTime.ServerTime - RenderLatency;

      int interpolateFromIndex = _snapshots.IndexOfSnapshotBeforeTime(renderTime);
      if (interpolateFromIndex != -1)
      {
        if (_snapshots.Count >= interpolateFromIndex + 1)
        {
          TransformSnapshot from = _snapshots[interpolateFromIndex];
          TransformSnapshot to = _snapshots[interpolateFromIndex+1];
          float t = Mathf.InverseLerp(from.SnapshotTime, to.SnapshotTime, renderTime);
          ApplySnapshot(TransformSnapshot.Lerp(from, to, t));
        }
        else
        {
          TransformSnapshot to = _snapshots[interpolateFromIndex];
          ApplySnapshot(to);
        }
      }
    }

    void ApplySnapshot(TransformSnapshot snapshot)
    {
      TargetTransform.SetPositionAndRotation(snapshot.Position, snapshot.Rotation);
      if (TargetTransform.localScale != snapshot.LocalScale)
        TargetTransform.localScale = snapshot.LocalScale;
    }
  }
}