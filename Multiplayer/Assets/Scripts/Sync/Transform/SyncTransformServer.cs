﻿using UnityEngine;
using UnityEngine.Networking;

namespace Sync
{
  public partial class SyncTransform
  {
    private float _nextSendTime = 1 / 20f;

    private void LateUpdate()
    {
      if (isServer)
        if (Time.time >= _nextSendTime && TargetTransform.hasChanged)
        {
          RpcSendSnapshot(new SyncTransform.TransformSnapshot(TargetTransform));
          TargetTransform.hasChanged = false;
          _nextSendTime += SendSnapshotsInterval;
        }
    }

    [Command]
    void CmdUpdateServer(TransformSnapshot clientTransform)
    {
      TargetTransform.SetPositionAndRotation(clientTransform.Position, clientTransform.Rotation);

      if (TargetTransform.localScale != clientTransform.LocalScale)
        TargetTransform.localScale = clientTransform.LocalScale;
    }
  }
}