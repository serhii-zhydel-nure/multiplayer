﻿using UnityEngine;
using UnityEngine.Networking;

namespace Sync
{
  public partial class SyncTransform : NetworkBehaviour
  {
    public const float SendSnapshotsInterval = 1 / 20f;
    public const float StoreSnapshotsTime= 2;
    public const float RenderLatency = 0.1f;

    public Transform TargetTransform;


    private readonly SnapshotCollection<TransformSnapshot> _snapshots
      = new SnapshotCollection<TransformSnapshot>
        (Mathf.RoundToInt(StoreSnapshotsTime/SendSnapshotsInterval));//snapshots capacity

    protected struct TransformSnapshot : ISnapshot
    {
      public Vector3 Position;
      public Quaternion Rotation;
      public Vector3 LocalScale;
      public float SnapshotTime { get; private set; }

      public TransformSnapshot(Transform transform) : this()
      {
        Position = transform.position;
        Rotation = transform.rotation;
        LocalScale = transform.localScale;
        SnapshotTime = SyncTime.ServerTime;
      }

      public static TransformSnapshot Lerp(TransformSnapshot from, TransformSnapshot to, float t)
      {
        TransformSnapshot result = new TransformSnapshot();
        result.Position = Vector3.Lerp(from.Position, to.Position, t);
        result.Rotation = Quaternion.Lerp(from.Rotation, to.Rotation, t);
        result.LocalScale = Vector3.Lerp(from.LocalScale, to.LocalScale, t);
        return result;
      }
    }

    #if UNITY_EDITOR
    private void OnValidate()
    {
      if (TargetTransform == null)
        TargetTransform = transform;
    }
    #endif
  }
}
