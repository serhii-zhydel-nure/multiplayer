﻿using System.Diagnostics;
using System.Linq;
using CrossProject.Extenders;
using UnityEngine;
using UnityEngine.Networking;

namespace Sync
{
  public partial class SyncTime
  {
    public float PingToClient { get { return _pingToClient; } }
    [SerializeField]
    private float _pingToClient;


    [Conditional("SERVER")]
    [Server]
    private void UpdateServerToClientPing()
    {
      if (isServer)
      {
        _pingToClient = GetPing(connectionToClient);
      }
    }

    [Command]
    private void CmdGetServerTime()
    {
      TargetSendServerTime(connectionToClient, Time.time);
    }
  }
}
