﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace Sync
{
  [DisallowMultipleComponent]
  public partial class SyncTime : NetworkBehaviour
  {
    public const float PingUpdateInterval = 0.5f;
    private float _nextUpdateTime = 0;

    private static SyncTime _instance;

    void Awake()
    {
      if (_instance != null)
      {
        //на клиенте, на чужом аватаре
        if (_instance.isClient && !_instance.isLocalPlayer)
          Destroy(_instance);
      }
      else
        _instance = this;
    }

    void Update()
    {
      if (Time.time >= _nextUpdateTime)
      {
        UpdateClientToServerPing();
        UpdateServerToClientPing();
        _nextUpdateTime += PingUpdateInterval;
      }
    }

    private float GetPing(NetworkConnection connection)
    {
      if (connection.hostId == -1)//it is client and server on 1 machine
        return 0;

      byte outError;
      return NetworkTransport.GetCurrentRTT
        (connection.hostId, connection.connectionId, out outError) 
             / 2f //for 1 side way 
             / 1000f;//in ms
    }

#if DEVELOPMENT_BUILD || UNITY_EDITOR
    private void OnGUI()
    {
      if (isLocalPlayer)
      {
        string text1 = String.Format("PingToServer: {0:F2} {2}S: {1:F2}",
          PingToServer, ServerTime, Environment.NewLine);

        GUI.Label(new Rect(Screen.width * 0.03f,
                            Screen.height * 0.03f,
                            Screen.width * 10f,
                            Screen.height * 20f), text1);
      }
    }

    private void OnDestroy()
    {
      Debug.Log(string.Format("Destroy {0}", name), gameObject);
    }
#endif
  }
}
