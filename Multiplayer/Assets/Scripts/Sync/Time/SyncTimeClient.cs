﻿using System.Diagnostics;
using System.Linq;
using CrossProject.Extenders;
using UnityEngine;
using UnityEngine.Networking;

namespace Sync
{
  public partial class SyncTime
  {
    public float PingToServer { get { return _pingToServer; } }
    [SerializeField]
    private float _pingToServer;

    public static float ServerTime
    { get { return _serverTimeDifference + Time.time; } }

    private static float _serverTimeDifference = 0;


    private NetworkClient _client;

    void Start()
    {
      var networkManager = FindObjectOfType<NetworkManager>();
      _client = networkManager.client;
    }

    [Conditional("CLIENT")]
    [Client]
    private void UpdateClientToServerPing()
    {
      CmdGetServerTime();
    }

    [TargetRpc]
    private void TargetSendServerTime(NetworkConnection connection, float serverTime)
    {
      _pingToServer = GetPing(_client.connection);
      _serverTimeDifference = serverTime - Time.time + PingToServer;
    }
  }
}