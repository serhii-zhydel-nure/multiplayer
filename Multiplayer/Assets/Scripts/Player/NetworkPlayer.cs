﻿using UnityEngine;
using UnityEngine.Networking;

public class NetworkPlayer : NetworkBehaviour
{
  public CharacterController CharacterController;
  private GameSceneController sceneController;

  private ETCJoystick joistick;
  // Use this for initialization
  public override void OnStartLocalPlayer()
  {
    sceneController = Camera.main.GetComponent<GameSceneController>();
    sceneController.ControlBinder.BindControl(transform);
    joistick = sceneController.ControlBinder.Joystick;
    Debug.Log("Control Binded!");
  }


  private Vector3 tmLastMove;

  [Command]
  public void CmdTakeInput(Vector2 input, float tmSpeed, float tmAdditionnalRotation, bool tmLockInJump)
  {
    float angle =Mathf.Atan2( input.x,input.y ) * Mathf.Rad2Deg;
    float speed = AnimationCurve.EaseInOut(0,0,1,1).Evaluate( new Vector2(input.x,input.y).magnitude) * tmSpeed;

    transform.rotation = Quaternion.Euler(new Vector3(0, angle + tmAdditionnalRotation, 0));

    if (CharacterController.isGrounded || !tmLockInJump)
    {
      Vector3 move = CharacterController.transform.TransformDirection(Vector3.forward) *  speed;
      CharacterController.Move(move * Time.deltaTime);
      tmLastMove = move;
    }
    else
    {
      CharacterController.Move(tmLastMove * Time.deltaTime);
    }
  }

  public void Update()
  {
    if (isLocalPlayer)
    {
      Vector2 input = new Vector2(joistick.axisX.axisValue, joistick.axisY.axisValue);
      CmdTakeInput(input, joistick.tmSpeed, joistick.tmAdditionnalRotation, joistick.tmLockInJump);
    }
  }
}
