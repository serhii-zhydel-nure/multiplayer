﻿using UnityEngine;

/// <summary>
/// Назначить джойстику трансформ, которым надо управлять
/// </summary>
public class ControlBinder : MonoBehaviour
{
    public ETCJoystick Joystick;

    /// <summary>
    /// Назначить джойстику трансформ, которым надо управлять
    /// </summary>
    public void BindControl(Transform controlledObj)
    {
        //Joystick.axisX.directTransform = controlledObj;
        Joystick.cameraTargetMode = ETCBase.CameraTargetMode.UserDefined;
        Joystick.cameraLookAt = controlledObj;

        gameObject.SetActive(controlledObj != null);
    }
 
}
