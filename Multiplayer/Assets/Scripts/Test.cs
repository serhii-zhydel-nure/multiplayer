﻿using UnityEngine;
using UnityEngine.Networking;

namespace DefaultNamespace
{
  public class Test : NetworkBehaviour
  {

    public override void OnStartClient()
    {
      Debug.Log(string.Format("{0}", "OnStartClient"));
      base.OnStartClient();
      byte errorClient;
      int clientRtt = NetworkTransport.GetCurrentRTT(connectionToClient.hostId, connectionToClient.connectionId, out errorClient);
      Debug.Log(string.Format("clientRtt rtt: {0}", clientRtt));

      byte errorServer;
      int serverRtt = NetworkTransport.GetCurrentRTT(connectionToServer.hostId, connectionToServer.connectionId, out errorServer);
      Debug.Log(string.Format("serverRtt rtt: {0}", serverRtt));
    }

    public override void OnStartServer()
    {
      base.OnStartServer();
      Debug.Log(string.Format("{0}", "OnStartServer"));
      base.OnStartClient();
      byte errorClient;
      int clientRtt = NetworkTransport.GetCurrentRTT(connectionToClient.hostId, connectionToClient.connectionId, out errorClient);
      Debug.Log(string.Format("clientRtt rtt: {0}", clientRtt));

      byte errorServer;
      int serverRtt = NetworkTransport.GetCurrentRTT(connectionToServer.hostId, connectionToServer.connectionId, out errorServer);
      Debug.Log(string.Format("serverRtt rtt: {0}", serverRtt));
    }
  }
}