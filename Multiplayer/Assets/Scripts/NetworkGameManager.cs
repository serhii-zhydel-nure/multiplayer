﻿using System.Collections.Generic;
using CrossProject.Extenders;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

namespace Sync
{
  public class NetworkGameManager : NetworkManager
  {

    public override void OnClientConnect(NetworkConnection conn)
    {
      base.OnClientConnect(conn);
      Debug.LogFormat("ClientConnected: {0}", conn);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
      base.OnClientDisconnect(conn);
      Debug.LogFormat("ClientDisconnected: {0}", conn);
    }

    public override void OnClientError(NetworkConnection conn, int errorCode)
    {
      base.OnClientError(conn, errorCode);
      Debug.LogFormat("OnClientError: {0} {1}", conn, (NetworkError)errorCode);
    }

    public override void OnClientNotReady(NetworkConnection conn)
    {
      base.OnClientNotReady(conn);
      Debug.LogFormat("ClientNotReady: {0}", conn);
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
      base.OnClientSceneChanged(conn);
      Debug.LogFormat("ClientSceneChanged: {0}", conn);
    }

    /// <summary>
    /// Called on the server when a client adds a new player with ClientScene.AddPlayer.
    /// The default implementation for this function creates 
    /// a new player object from the playerPrefab.
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="playerControllerId"></param>
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
      base.OnServerAddPlayer(conn, playerControllerId);
      Debug.LogFormat("ServerAddPlayer: {0} {1}", conn, playerControllerId);
    }

    /// <summary>
    /// Called on the server when a client adds a new player with ClientScene.AddPlayer.
    /// The default implementation for this function creates 
    /// a new player object from the playerPrefab.
    /// </summary>
    /// <param name="conn"></param>
    /// <param name="playerControllerId"></param>
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
    {
      base.OnServerAddPlayer(conn, playerControllerId, extraMessageReader);
      Debug.LogFormat("ServerAddPlayerExtra: {0} {1} {2}", conn, playerControllerId, extraMessageReader);
    }

    /// <summary>
    /// Called on the server when a new client connects
    /// </summary>
    /// <param name="conn"></param>
    public override void OnServerConnect(NetworkConnection conn)
    {
      base.OnServerConnect(conn);
      Debug.LogFormat("ServerConnect: {0}", conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
      base.OnServerDisconnect(conn);
      Debug.LogFormat("ServerDisconnect: {0}", conn);
    }

    public override void OnServerError(NetworkConnection conn, int errorCode)
    {
      base.OnServerError(conn, errorCode);
      Debug.LogFormat("ServerError: {0} {1}", conn, (NetworkError)errorCode);
    }

    /// <summary>
    /// Called on the server when a client is ready.
    /// The default implementation of this function calls
    /// NetworkServer.SetClientReady() to continue the network setup process
    /// </summary>
    /// <param name="conn"></param>
    public override void OnServerReady(NetworkConnection conn)
    {
      base.OnServerReady(conn);
      Debug.LogFormat("ServerReady: {0}", conn);
    }

    public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
    {
      base.OnServerRemovePlayer(conn, player);
      Debug.LogFormat("ServerRemovePlayer: {0} {1}", conn, player);
    }

    /// <summary>
    /// This causes the server to switch scenes and sets the networkSceneName.
    /// Clients that connect to this server will automatically switch to this scene.
    /// This is called autmatically if onlineScene or offlineScene are set, 
    /// but it canbe called from user code to switch scenes again 
    /// while the game is in progress.This automatically sets clients to be not-ready. 
    /// The clients must call NetworkClient.Ready() again to participate in the new scene
    /// </summary>
    /// <param name="newSceneName"></param>
    public override void ServerChangeScene(string newSceneName)
    {
      base.ServerChangeScene(newSceneName);
      Debug.LogFormat("ServerChangeScene: {0}", newSceneName);
    }

    /// <summary>
    /// Called on the server when a scene is completed loaded, 
    /// when the scene load initiated by the server with ServerChangeScene()
    /// </summary>
    /// <param name="sceneName"></param>
    public override void OnServerSceneChanged(string sceneName)
    {
      base.OnServerSceneChanged(sceneName);
      Debug.LogFormat("ServerSceneChanged: {0}", sceneName);
    }

    public override void OnStartHost()
    {
      base.OnStartHost();
      Debug.LogFormat("OnStartHost");
    }

    public override void OnStopHost()
    {
      base.OnStopHost();
      Debug.LogFormat("OnStopHost");
    }

    public override NetworkClient StartHost()
    {
      Debug.LogFormat("StartHost");
      return base.StartHost();
    }

    public override NetworkClient StartHost(ConnectionConfig config, int maxConnections)
    {
      Debug.LogFormat("StartHost: {0} {1}", config, maxConnections);
      return base.StartHost(config, maxConnections);
    }

    public override void OnStartServer()
    {
      base.OnStartServer();
      Debug.LogFormat("StartServer: {0}", NetworkServer.serverHostId);
    }

    public override void OnStopServer()
    {
      base.OnStopServer();
      Debug.LogFormat("StopServer");
    }

    public override void OnStartClient(NetworkClient client)
    {
      base.OnStartClient(client);
      Debug.LogFormat("StartClient: {0}", client);
    }

    public override void OnStopClient()
    {
      base.OnStopClient();
      Debug.LogFormat("StopClient");
    }



    public override void OnDestroyMatch(bool success, string extendedInfo)
    {
      base.OnDestroyMatch(success, extendedInfo);
      Debug.LogFormat("DestroyMatch: {0} {1}", success, extendedInfo);
    }

    public override void OnDropConnection(bool success, string extendedInfo)
    {
      base.OnDropConnection(success, extendedInfo);
      Debug.LogFormat("DropConnection: {0} {1}", success, extendedInfo);
    }

    public override void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
      base.OnMatchCreate(success, extendedInfo, matchInfo);
      Debug.LogFormat("MatchCreate: {0} {1} {2}", success, extendedInfo, matchInfo);
    }

    public override void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
    {
      base.OnMatchJoined(success, extendedInfo, matchInfo);
      Debug.LogFormat("MatchJoined: {0} {1} {2}", success, extendedInfo, matchInfo);
    }

    public override void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {
      base.OnMatchList(success, extendedInfo, matchList);
      Debug.LogFormat("MatchList: {0} {1} {2}", success, extendedInfo, matchList.ToStringAll());
    }

    public override void OnSetMatchAttributes(bool success, string extendedInfo)
    {
      base.OnSetMatchAttributes(success, extendedInfo);
      Debug.LogFormat("SetMatchAttributes: {0} {1}", success, extendedInfo);
    }


  }
}